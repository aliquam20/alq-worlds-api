package org.aliquam.worlds.api.model;

public enum AlqWorldType {
    NORMAL,
    FLAT,
    LARGE_BIOMES,
    AMPLIFIED;
}
