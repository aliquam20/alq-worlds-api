package org.aliquam.worlds.api.model;

public enum AlqWorldMemberStatus {
    /**
     * WorldMember is registered in AlqWorlds but not in AlqPerms yet.
     * Usually this status should quickly change to "OK" unless AlqPerms is down.
     * TODO: Create scheduler for that
     */
    PENDING_ADD,

    /**
     * Everything is OK :)
     */
    OK,

    /**
     * WorldMember is registered for deletion in AlqWorlds but not in AlqPerms yet.
     * Usually the members should be quickly deleted unless AlqPerms is down. In such case AlqWorlds will retry later.
     */
    PENDING_DELETE
}