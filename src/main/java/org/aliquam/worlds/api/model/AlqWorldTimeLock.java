package org.aliquam.worlds.api.model;

public enum AlqWorldTimeLock {
    Normal,
    Day,
    Night,
    // TODO: Realtime, Fixed(manual date)
}
