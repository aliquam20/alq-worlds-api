package org.aliquam.worlds.api.model;

public enum AlqWorldDifficulty {
    Peaceful,
    Easy,
    Normal,
    Hard,
}
