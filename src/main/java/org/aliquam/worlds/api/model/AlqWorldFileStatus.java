package org.aliquam.worlds.api.model;

public enum AlqWorldFileStatus {

    /**
     * The initial, temporary state of the file.
     * Usually transitions into "COMMITTED" after no more than a few minutes.
     * Can stay in this state in case of some errors.
     * If this status persists for a longer time (an hour or so) then this entry will be automatically transitioned to WAITING_FOR_DELETE.
     */
    QUEUED,

// Maybe add at some point...
//    GENERATING,

    /**
     * Stored file is verified to be correct (not corrupted).
     * File can be used by AlqWorld
     */
    COMMITTED,

    /**
     * Sadly we cannot specify single transaction for both database and filesystem...
     * To overcome this issue the file deletion is done in 3 steps:
     * - delete AlqWorldFile reference from AlqWorld and mark AlqWorldFile for deletion
     * - delete .7z file
     * - delete AlqWorldFile model
     * Usually all these steps are done within the same "DELETE" request thus this status rarely happen on production,
     * But in case of IOException happens at stage #2 we can recover later with a scheduled cleanup job.
     */
    WAITING_FOR_DELETE;
}
