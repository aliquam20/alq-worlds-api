package org.aliquam.worlds.api.model;

public enum AlqWorldMemberType {
//    Default,
//    Banned,
    Whitelisted,
    WhitelistedPlus,
    Member,
    Mod,
    Admin
}
