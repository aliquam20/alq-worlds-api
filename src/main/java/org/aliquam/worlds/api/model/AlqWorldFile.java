package org.aliquam.worlds.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqWorldFile {
    private AlqWorldFileStatus status;
    private Instant createdAt;
    private Instant usedAt;

    /**
     * Server that used this file last.
     * Show a warning "changes are not synchronized. Are you sure you want to use the state from the cloud?"
     * Actually, screw it... It should be managed with world locks instead...
     */
//    private UUID usedBy;

    private AlqWorldGenerator generator;
}
