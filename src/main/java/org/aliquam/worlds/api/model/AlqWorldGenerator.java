package org.aliquam.worlds.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AlqWorldGenerator {
    private String generator;
    private AlqWorldEnvironment environment;
    private AlqWorldType type;
    private long seed;
    private boolean generateStructures;
    private boolean hardcore;

//    @NotNull
    private String rawGeneratorSettings;
}
