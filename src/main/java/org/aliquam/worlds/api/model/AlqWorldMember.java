package org.aliquam.worlds.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqWorldMember {
    private UUID world;
    private UUID player;
    private AlqWorldMemberType type;
}
