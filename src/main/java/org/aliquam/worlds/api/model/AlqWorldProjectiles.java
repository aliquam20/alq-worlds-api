package org.aliquam.worlds.api.model;

public enum AlqWorldProjectiles {
    Allowed,
    NotAllowed,
    MembersOnly
}
