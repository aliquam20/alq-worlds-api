package org.aliquam.worlds.api.model;

public enum AlqWorldAccess {
    Private,
    Public,
}
