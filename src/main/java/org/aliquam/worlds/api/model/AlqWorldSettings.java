package org.aliquam.worlds.api.model;

import lombok.Data;

import java.util.UUID;

@Data
public class AlqWorldSettings {
    private AlqWorldAccess access;
    private AlqWorldChat chat;
    private UUID privateChatGroup;
    private AlqWorldGamemode gamemode;
    private AlqWorldSpawn spawn;
    private boolean findSafeSpawnLocation;

    private AlqWorldTimeLock timeLock;
    private AlqWorldWeather weather;
    private boolean physics;
    private boolean mobs;
    private boolean interaction;
    private String resourcepack;
    private String welcomeMessage;
    private String farewellMessage;

    private AlqWorldDifficulty difficulty;
    // TODO: private boolean hardcore; ???
    private boolean drops;
    private AlqWorldProjectiles projectiles;
    private boolean explosions;
    private boolean healthRegen;
    private boolean keepInventory;
    private UUID survivalInventoryGroup;
    private boolean consume;
    private boolean pvp;
    private boolean fire;

    private boolean autoUnload;
    private boolean autoBorder;
    private boolean autoTrimming;
    private Integer keepLocallyForDays;
    private int mobLimit;
}
