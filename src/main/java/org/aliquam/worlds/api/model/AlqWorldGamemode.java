package org.aliquam.worlds.api.model;

public enum AlqWorldGamemode {
    Creative,
    Survival,
    Adventure,
    Spectator,
}
