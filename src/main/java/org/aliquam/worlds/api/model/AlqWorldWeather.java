package org.aliquam.worlds.api.model;

public enum AlqWorldWeather {
    Normal,
    Sun,
    Rain,
    Storm
}
