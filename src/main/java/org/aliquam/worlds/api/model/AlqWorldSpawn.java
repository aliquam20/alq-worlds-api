package org.aliquam.worlds.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqWorldSpawn {
    private double spawnX;
    private double spawnY;
    private double spawnZ;
    private double spawnPitch;
    private double spawnYaw;
}
