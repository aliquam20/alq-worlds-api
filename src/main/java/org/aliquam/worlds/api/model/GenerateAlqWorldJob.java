package org.aliquam.worlds.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenerateAlqWorldJob {
    private UUID world;
    private AlqWorldGenerator generator;
}
