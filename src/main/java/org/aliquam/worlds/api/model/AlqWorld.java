package org.aliquam.worlds.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqWorld {
    private UUID id;
    private UUID owner;
    private String name;

    private AlqWorldSettings settings;
    private AlqWorldFile file;
}
