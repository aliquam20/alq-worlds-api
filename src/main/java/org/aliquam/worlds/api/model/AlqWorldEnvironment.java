package org.aliquam.worlds.api.model;

public enum AlqWorldEnvironment {
    NORMAL,
    NETHER,
    THE_END,
    CUSTOM;
}
