package org.aliquam.worlds.api.connector;

import lombok.SneakyThrows;
import org.aliquam.cum.network.AlqRequestBuilder;
import org.aliquam.cum.network.AlqUrl;
import org.aliquam.cum.network.handlers.InputStreamResponseHandler;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.aliquam.worlds.api.model.GenerateAlqWorldJob;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

public class AlqWorldsFileConnector {

    private final AlqSessionManager sessionManager;

    public AlqWorldsFileConnector(AlqSessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    private static AlqUrl fileResource(UUID id) {
        return AlqWorldsConnector.worldResource.resolve(id).resolve("file");
    }

    @SneakyThrows
    private InputStream getInputStream(Path archive) {
        return Files.newInputStream(archive);
    }

    public void generateWorldFile(GenerateAlqWorldJob jobRequest) {
        generateWorldFile(jobRequest, true);
    }

    /**
     * @param jobRequest generation settings
     * @param generate whether to queue the request to AlqGenerator.
     *                 If false, the world file has to be manually committed
     */
    public void generateWorldFile(GenerateAlqWorldJob jobRequest, boolean generate) {
        new AlqRequestBuilder(fileResource(jobRequest.getWorld()).resolve("generate"))
                .acceptApplicationJson()
                .header("generate", Boolean.toString(generate))
                .includeSession(sessionManager)
                .withRetries()
                .POST(jobRequest)
                .build().execute();
    }

    @SneakyThrows
    public void commitGeneratedWorldFile(UUID id, Path inputFile) {
        new AlqRequestBuilder(fileResource(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST_file(inputFile)
                .build().execute();
    }

    @SneakyThrows
    public void getWorldFile(UUID id, Path targetFile) {
        try(InputStream fileStream = getWorldFile(id)) {
            Files.copy(fileStream, targetFile, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public InputStream getWorldFile(UUID id) {
        return new AlqRequestBuilder(fileResource(id))
                .acceptApplicationOctetStream()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new InputStreamResponseHandler());
    }

    @SneakyThrows
    public void updateWorldFile(UUID id, Path inputFile, boolean incremental) {
        new AlqRequestBuilder(fileResource(id)
                .queryParam("incremental", incremental))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .PUT_file(inputFile)
                .build().execute();
    }

    public void deleteWorldFile(UUID id) {
        new AlqRequestBuilder(fileResource(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .DELETE()
                .build().execute();
    }
}
