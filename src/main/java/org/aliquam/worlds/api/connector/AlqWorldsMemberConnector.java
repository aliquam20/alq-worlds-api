package org.aliquam.worlds.api.connector;

import com.fasterxml.jackson.core.type.TypeReference;
import org.aliquam.cum.network.AlqRequestBuilder;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.aliquam.worlds.api.communication.PutWorldMemberRequest;
import org.aliquam.worlds.api.model.AlqWorld;
import org.aliquam.worlds.api.model.AlqWorldMember;
import org.aliquam.worlds.api.model.AlqWorldMemberType;

import java.util.List;
import java.util.UUID;

public class AlqWorldsMemberConnector {

    private final AlqSessionManager sessionManager;

    public AlqWorldsMemberConnector(AlqSessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public List<AlqWorldMember> listMembers(AlqWorld world) {
        return listMembers(world.getId());
    }

    public List<AlqWorldMember> listMembers(UUID world) {
        return new AlqRequestBuilder(AlqWorldsConnector.worldResource.resolve(world).resolve("members"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<AlqWorldMember>>() {});
    }

    public void addMember(AlqWorld world, UUID player, AlqWorldMemberType type) {
        addMember(world.getId(), player, type);
    }

    public void addMember(UUID world, UUID player, AlqWorldMemberType type) {
        new AlqRequestBuilder(AlqWorldsConnector.worldResource.resolve(world).resolve("members").resolve(player).resolve("add"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(new PutWorldMemberRequest(type))
                .build().execute();
    }

    public void delMember(AlqWorld world, UUID player) {
        delMember(world.getId(), player);
    }

    public void delMember(UUID world, UUID player) {
        new AlqRequestBuilder(AlqWorldsConnector.worldResource.resolve(world).resolve("members").resolve(player))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .DELETE()
                .build().execute();
    }

}
