package org.aliquam.worlds.api.connector;

import org.aliquam.cum.network.AlqConnector;
import org.aliquam.cum.network.AlqRequestBuilder;
import org.aliquam.cum.network.AlqUrl;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.aliquam.worlds.api.communication.CreateWorldRequest;
import org.aliquam.worlds.api.communication.PatchWorldRequest;
import org.aliquam.worlds.api.model.AlqWorld;

import java.util.UUID;

public class AlqWorldsConnector {
    private static final String DEFAULT_WORLD_NAME = "world";
    private static final AlqUrl alqWorldsService = AlqConnector.of("worlds");
//    private static final WebTarget adminResource = alqWorldsService.path("admin");
    public static final AlqUrl worldResource = alqWorldsService.resolve("world");

    private final AlqSessionManager sessionManager;

    public AlqWorldsConnector(AlqSessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public AlqWorld createWorld(UUID id, UUID ownerId) {
        return createWorld(new CreateWorldRequest(id, ownerId, DEFAULT_WORLD_NAME));
    }

    public AlqWorld createWorld(UUID id, UUID ownerId, String name) {
        return createWorld(new CreateWorldRequest(id, ownerId, name));
    }

    public AlqWorld createWorld(CreateWorldRequest request) {
        return new AlqRequestBuilder(worldResource)
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(request)
                .build().execute(AlqWorld.class);
    }

    public AlqWorld getWorld(UUID id) {
        return new AlqRequestBuilder(worldResource.resolve(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(AlqWorld.class);
    }

    public AlqWorld updateWorld(UUID id, PatchWorldRequest request) {
        return new AlqRequestBuilder(worldResource.resolve(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .PATCH(request)
                .build().execute(AlqWorld.class);
    }

    public void deleteWorld(UUID id) {
        new AlqRequestBuilder(worldResource.resolve(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .DELETE()
                .build().execute();
    }
}
