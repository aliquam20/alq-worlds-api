package org.aliquam.worlds.api.communication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateWorldRequest {
    /**
     * Optional, keep null to autogenerate
     */
    private UUID id;

    private UUID owner;

    private String name;
}
