package org.aliquam.worlds.api.communication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aliquam.worlds.api.model.AlqWorldAccess;
import org.aliquam.worlds.api.model.AlqWorldChat;
import org.aliquam.worlds.api.model.AlqWorldDifficulty;
import org.aliquam.worlds.api.model.AlqWorldEnvironment;
import org.aliquam.worlds.api.model.AlqWorldGamemode;
import org.aliquam.worlds.api.model.AlqWorldProjectiles;
import org.aliquam.worlds.api.model.AlqWorldSpawn;
import org.aliquam.worlds.api.model.AlqWorldTimeLock;
import org.aliquam.worlds.api.model.AlqWorldType;
import org.aliquam.worlds.api.model.AlqWorldWeather;

import java.util.UUID;

@Data
public class PatchWorldRequest {
    private String name;
//    private Generator generator = new Generator();
    private Settings settings = new Settings();

//    @Data
//    public static class Generator {
//        private String generator;
//        private AlqWorldEnvironment environment;
//        private AlqWorldType type;
//        private Long seed;
//        private Boolean generateStructures;
//        private Boolean hardcore;
//        private String rawGeneratorSettings;
//    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SettingsSpawn {
        private Double spawnX;
        private Double spawnY;
        private Double spawnZ;
        private Double spawnPitch;
        private Double spawnYaw;
    }

    @Data
    public static class Settings {
        private AlqWorldAccess access;
        private AlqWorldChat chat;
        private UUID privateChatGroup;
        private AlqWorldGamemode gamemode;
        private SettingsSpawn spawn;
        private Boolean findSafeSpawnLocation;

        private AlqWorldTimeLock timeLock;
        private AlqWorldWeather weather;
        private Boolean physics;
        private Boolean mobs;
        private Boolean interaction;
        private String resourcepack;
        private String welcomeMessage;
        private String farewellMessage;

        private AlqWorldDifficulty difficulty;
        // TODO: private boolean hardcore; ???
        private Boolean drops;
        private AlqWorldProjectiles projectiles;
        private Boolean explosions;
        private Boolean healthRegen;
        private Boolean keepInventory;
        private UUID survivalInventoryGroup;
        private Boolean consume;
        private Boolean pvp;
        private Boolean fire;

        private Boolean autoUnload;
        private Boolean autoBorder;
        private Boolean autoTrimming;
        private Integer keepLocallyForDays;
        private Integer mobLimit;

    }

}
