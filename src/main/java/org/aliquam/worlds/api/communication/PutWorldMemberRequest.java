package org.aliquam.worlds.api.communication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aliquam.worlds.api.model.AlqWorldMemberType;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PutWorldMemberRequest {
    private AlqWorldMemberType type;
}
